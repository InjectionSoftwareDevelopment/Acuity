#include "search.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Search w;
    w.show();

    QPixmap icon(":/128.png");
    QApplication::setWindowIcon(icon);

    return a.exec();
}
