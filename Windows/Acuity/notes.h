#ifndef NOTES_H
#define NOTES_H

#include <iostream>
#include <QMainWindow>
#include <QPushButton>
#include <QFile>
#include <QMessageBox>
#include <QTextStream>
#include <QTextEdit>
#include <QStandardPaths>
#include <QDir>
#include <QSpacerItem>
#include <QGridLayout>

using namespace std;

namespace Ui {
class notes;
}

class notes : public QMainWindow
{
    Q_OBJECT

public:
    explicit notes(QWidget *parent = 0);
    ~notes();

private slots:
    int on_saveNotes_clicked();
    void closeEvent (QCloseEvent *event);


private:
    Ui::notes *ui;

    QPushButton * saveNotes;

    QTextEdit * textEdit = new QTextEdit(this);

    bool didSave;



};

#endif // NOTES_H
