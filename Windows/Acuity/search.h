#ifndef SEARCH_H
#define SEARCH_H

#include <QMainWindow>
#include <QWebView>
#include <QtNetwork>
#include <QtWebKit>
#include <QtWebKitWidgets/QWebFrame>
#include <QPushButton>
#include <QMenu>
#include <QStatusBar>
#include "notes.h"
#include "citation.h"

namespace Ui {
class Search;
}

class Search : public QMainWindow
{
    Q_OBJECT

public:
    explicit Search(QWidget *parent = 0);
    ~Search();

private slots:

    void on_webView_loadFinished(bool arg1);

    void on_searchBar_returnPressed();

    void selectWiki();

    void selectDict();

    void selectEncyc();

    void on_sourceButton_clicked();

    void on_noteButton_clicked();

    void on_citationButton_clicked();

    void closeEvent (QCloseEvent *event);

private:
    Ui::Search *ui;

    notes *notesWindow;
    citation * citationWindow;

    QString searchedText;
    QString url;

    QPushButton * sourceButton;
    QPushButton * noteButton;
    QPushButton * citationButton;

    QMenu * sourceMenu;
    QAction * wikiButton;
    QAction * dictButton;
    QAction * encycButton;



    bool wiki;
    bool ency;
    bool dict;

};


#endif // SEARCH_H
