#-------------------------------------------------
#
# Project created by QtCreator 2015-09-12T13:36:36
#
#-------------------------------------------------

QT       += core gui
QT       += webkit
QT       += webkitwidgets
QT       += network


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

RC_FILE = resources.rc
VERSION = 1.0

TARGET = Acuity
TEMPLATE = app


SOURCES += main.cpp\
        search.cpp \
    notes.cpp \
    citation.cpp

HEADERS  += search.h \
    notes.h \
    citation.h

FORMS    += \
    search.ui \
    notes.ui \
    citation.ui

RESOURCES += \
    resources.qrc
