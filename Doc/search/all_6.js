var searchData=
[
  ['on_5fapabutton_5fclicked',['on_apaButton_clicked',['../classcitation.html#aa7026c1fa6d4340d2b784b7ab34d04bd',1,'citation']]],
  ['on_5fcitationbutton_5fclicked',['on_citationButton_clicked',['../class_search.html#a5839bbebba5cafedfccb7a153524cdb4',1,'Search']]],
  ['on_5fgeneratecitation_5fclicked',['on_generateCitation_clicked',['../classcitation.html#ab2eec38970e6a9ed8e7b782c5b9c5855',1,'citation']]],
  ['on_5fmlabutton_5fclicked',['on_mlaButton_clicked',['../classcitation.html#a3e994910e261056a8e25e22554e8fc7b',1,'citation']]],
  ['on_5fnotebutton_5fclicked',['on_noteButton_clicked',['../class_search.html#a98d7ef2062e64c091fa112206aff7186',1,'Search']]],
  ['on_5fsavenotes_5fclicked',['on_saveNotes_clicked',['../classnotes.html#ab682e7a1e6b5d4abc3c125e7e3a5b336',1,'notes']]],
  ['on_5fsearchbar_5freturnpressed',['on_searchBar_returnPressed',['../class_search.html#a74ec5b2609e3fe6c48206d547d4187e9',1,'Search']]],
  ['on_5fsourcebutton_5fclicked',['on_sourceButton_clicked',['../class_search.html#af3b51040601e0ea440b48a3908ff7a9e',1,'Search']]],
  ['on_5fsourceselectbutton_5fclicked',['on_sourceSelectButton_clicked',['../classcitation.html#abe07a9c7d7399196ba7c51deda1cfdd0',1,'citation']]],
  ['on_5fwebview_5floadfinished',['on_webView_loadFinished',['../classcitation.html#a37235d5b9585ebdf73e6e217ff22cbe3',1,'citation::on_webView_loadFinished()'],['../class_search.html#a77b354c078da88329e120d043351d19f',1,'Search::on_webView_loadFinished()']]]
];
