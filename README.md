# Acuity
Repository for Acuity: Learning Resource. Open Source C++ project.
==================================================================


# Documentation:
http://doc.injecti0n.org/acuity/

A copy of the documentation is also included with this repository when you download or clone it. It is located in the Doc folder and can be accessed by opening the index.html file in a web browser.


App Installation Guide (Not from source):
-----------------------------------------

#### Windows users use the the installation wizard at https://injecti0n.org/downloads/

#### Ubuntu/Debian Linux users .deb from: https://injecti0n.org/downloads/

***ONLY UBUNTU 14.04 AND ITS OFFICIAL DERIVATIVES ARE OFFICIALLY SUPPORTED BY THIS PROJECT***

>(Debian based distros should work, but are not officially supported. You are on your own when troubleshooting those issues.)

#### If you have installation issue on Ubuntu 14.04 make sure you open terminal and run:

>sudo apt-get update && sudo apt-get upgrade

This will update the ubuntu repositories to allow Acuity to obtain the dependecies it needs. No dependencies *should* be absent from
Ubuntu's default repositories.

### All Dependecies are from Qt 5.5 and will be listed below:

To install Qt 5.5 visit http://www.qt.io/download/

#### Windows:


>icudt54.dll
>
>icuin54.dll
>
>icuuc54.dll
>
>libeay32.dll
>
>libgcc_s_dw2-1.dll
>
>libstdc++-6.dll
>
>libwinpthread-1.dll
>
>Qt5Core.dll
>
>Qt5Gui.dll
>
>Qt5Multimedia.dll
>
>Qt5MultimediaWidgets.dll
>
>Qt5Network.dll
>
>Qt5OpenGL.dll
>
>Qt5Positioning.dll
>
>Qt5PrintSupport.dll
>
>Qt5Qml.dll
>
>Qt5Quick.dll
>
>Qt5Sensors.dll
>
>Qt5Sql.dll
>
>Qt5WebChannel.dll
>
>Qt5Webkit.dll
>
>Qt5WebkitWidgets.dll
>
>Qt5Widgets.dll
>
>ssleay32.dll
>
>qwindows.dll


#### Linux:


>libqt5widgets5
>
>libqt5gui5
>
>libqt5network5
>
>qt5-default
>
>libqt5webkit5-dev
>
>qt5-qmake


***All of the dependencies listed above are either included with the packaged binary, or are downloaded when installing Acuity***
