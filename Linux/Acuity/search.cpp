#include "search.h"
#include "ui_search.h"


Search::Search(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Search)
{
    ui->setupUi(this);
    this->setFixedSize(QSize(764, 480));
    //Hide webview for background web engine//
    ui->webView->hide();
    ui->sourceButton->setStyleSheet("background-color: #ffffff; border-radius: 5; color: white");
    ui->noteButton->setStyleSheet("background-color: #ffffff; border-radius: 5; color: white");
    ui->citationButton->setStyleSheet("background-color: #ffffff; border-radius: 5; color: white");
    ui->searchBar->setStyleSheet("border-radius: 5;");

    //wiki as default search//
        wiki = true;

     //setup extra main window attributes and object stypes//

    setWindowTitle("Acuity");


    QPixmap sourceIcon(":/sourceicon.png");
    QIcon SourceButtonIcon(sourceIcon);
    ui->sourceButton->setIcon(SourceButtonIcon);
    ui->sourceButton->setIconSize(QSize(25,25));


    QPixmap noteIcon(":/composenote.png");
    QIcon NoteButtonIcon(noteIcon);
    ui->noteButton->setIcon(NoteButtonIcon);
    ui->noteButton->setIconSize(QSize(25,25));

    QPixmap citeIcon(":/quotationmarks.png");
    QIcon CiteButtonIcon(citeIcon);
    ui->citationButton->setIcon(CiteButtonIcon);
    ui->citationButton->setIconSize(QSize(30,25));

}

Search::~Search()
{
    delete ui;

}




void Search::closeEvent (QCloseEvent *event){

    QApplication::quit();

}



//Parse HTML//
void Search::on_webView_loadFinished(bool arg1)
{
    QWebElementCollection extract = ui->webView->page()->mainFrame()->findAllElements("body");

    foreach(QWebElement elemento, extract){
        ui->mainText->setText(elemento.toPlainText());
    }

}

//Search and Sources//
void Search::on_searchBar_returnPressed()
{
    searchedText = ui->searchBar->text();


   if (wiki == true){
        url = "http://en.wikipedia.org/wiki/";
        searchedText.replace(" ", "_");
        url.append(searchedText);

   }else if(ency == true){

     url = "http://www.encyclopedia.com/topic/";
    searchedText.replace(" ", "%20");
    url.append(searchedText).append(".aspx");

   }else if(dict == true){

       url = "http://dictionary.reference.com/browse/";
       searchedText.replace(" ", "_");
       url.append(searchedText);

   }

    ui->webView->load(QUrl(url));
}




void Search::selectWiki(){

    wiki = true;
    ency = false;
    dict = false;

    on_searchBar_returnPressed();

}

void Search::selectDict(){

    wiki = false;
    ency = false;
    dict = true;

    on_searchBar_returnPressed();

}


void Search::selectEncyc(){

    wiki = false;
    ency = true;
    dict = false;

    on_searchBar_returnPressed();
}


//Select Sources//
void Search::on_sourceButton_clicked(){

    //Makes QMenu object
sourceMenu = new QMenu();


//Set context, add action buttons to menu, and enable a pop-up//
setContextMenuPolicy(Qt::CustomContextMenu);


wikiButton = sourceMenu->addAction("Wikipedia");
dictButton = sourceMenu->addAction("Dictionary");
encycButton = sourceMenu->addAction("Encyclopedia");
connect(this,SIGNAL(customContextMenuRequested(const QPoint)),this,SLOT(contextMenuRequested(QPoint)));
connect(wikiButton,SIGNAL(triggered()),this,SLOT(selectWiki()));
connect(dictButton,SIGNAL(triggered()),this,SLOT(selectDict()));
connect(encycButton,SIGNAL(triggered()),this,SLOT(selectEncyc()));

     QPoint menuPoint = ui->sourceButton->pos();

    sourceMenu->popup(mapToGlobal(menuPoint));
}

void Search::on_noteButton_clicked()
{
    notesWindow = new notes();
    notesWindow->show();
}

void Search::on_citationButton_clicked()
{
    citationWindow = new citation();
    citationWindow->show();

}
