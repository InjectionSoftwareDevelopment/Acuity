#include "notes.h"
#include "ui_notes.h"

notes::notes(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::notes)
{
    ui->setupUi(this);
    this->setFixedSize(QSize(764, 480));
    ui->saveNotes->setStyleSheet("background-color: #ffffff; border-radius: 5; color: white");

    setWindowTitle("Notes");

    QPixmap saveIcon(":/saveicon.png");
    QIcon SaveButtonIcon(saveIcon);
    ui->saveNotes->setIcon(SaveButtonIcon);
    ui->saveNotes->setIconSize(QSize(50,27));

//Set location to local app data folder. Cross Platform.
    QString location =
        QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);

    QDir::setCurrent(location);
//Read whatever that file is. Should always be constant, whatever the user saved last is what will appear.
    QFile file(location + "Notes.txt");
       if(!file.open(QFile::ReadOnly |
                     QFile::Text))
       {
           cout << "And error occured!" << endl;
       }

       QTextStream in(&file);
       QString myText = in.readAll();
        //Set the text that was read to the text view
       ui->textEdit->setText(myText);
       //Close the file stream
       file.close();

       didSave = false;
}




notes::~notes()
{
    delete ui;
}

int notes::on_saveNotes_clicked()
{

    //Sets the path to their local app data folder. Cross Platform
    QString location =
        QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);

    QDir::setCurrent(location);

    //Write our file to that location
    QFile file(location + "Notes.txt");
         if (!file.open(QIODevice::WriteOnly | QIODevice::Text)){
             return 1;
    }
         QTextStream out(&file);
         out << ui->textEdit->toPlainText();

         //Pop up to let user know their information saved
         QMessageBox msgBox;
         msgBox.setText("Information Saved!");
         msgBox.setInformativeText("Your information was saved!");
         msgBox.setDefaultButton(QMessageBox::Ok);
         QPixmap qMIcon(":/128.png");
         QPixmap messageIcon = qMIcon.scaled(QSize(128,128));
         msgBox.setIconPixmap(messageIcon);

         QSpacerItem* horizontalSpacer = new QSpacerItem(400, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);
         QGridLayout* layout = (QGridLayout*)msgBox.layout();
         layout->addItem(horizontalSpacer, layout->rowCount(), 0, 1, layout->columnCount());

         msgBox.exec();

        //Close file stream after user recieves pop-up
         file.close();

         didSave = true;
}

void notes::closeEvent (QCloseEvent *event){

    if (didSave != true){
        QMessageBox msgBox;
        msgBox.setText("Would you like to save your notes?");
        msgBox.setStandardButtons(QMessageBox::No | QMessageBox::Yes);
        msgBox.setDefaultButton(QMessageBox::Yes);
        QPixmap qMIcon(":/128.png");
        QPixmap messageIcon = qMIcon.scaled(QSize(128,128));
        msgBox.setIconPixmap(messageIcon);

        QSpacerItem* horizontalSpacer = new QSpacerItem(400, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);
        QGridLayout* layout = (QGridLayout*)msgBox.layout();
        layout->addItem(horizontalSpacer, layout->rowCount(), 0, 1, layout->columnCount());

        if(msgBox.exec() == QMessageBox::Yes){

            //Sets the path to their local app data folder. Cross Platform
            QString location =
                QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);

            QDir::setCurrent(location);

            //Write our file to that location
            QFile file(location + "Notes.txt");
                 if (!file.open(QIODevice::WriteOnly | QIODevice::Text)){
                     cout << "And error occured" << endl;
            }
                 QTextStream out(&file);
                 out << ui->textEdit->toPlainText();

                  file.close();

        }else{

            cout << "The user chose not to save..." << endl;

        }

    }else{

        cout << "The user already saved!" << endl;
    }
}


