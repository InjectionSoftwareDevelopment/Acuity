#-------------------------------------------------
#
# Project created by QtCreator 2015-07-16T17:09:30
#
#-------------------------------------------------

QT       += core gui
QT       += webkit
QT       += webkitwidgets
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Acuity
TEMPLATE = app



ICON = /usr/share/pixmaps/acuity.ico

INCLUDEPATH = .

target.path = /usr/share/Acuity/
shortcutfiles.files += acuity.desktop
shortcutfiles.path =/usr/share/applications/
data.files = acuity.ico
data.path = /usr/share/pixmaps/


INSTALLS += target

INSTALLS += shortcutfiles

INSTALLS += data


SOURCES += main.cpp\
        search.cpp \
    notes.cpp \
    citation.cpp

HEADERS  += search.h \
    notes.h \
    citation.h

FORMS    += search.ui \
    notes.ui \
    citation.ui

RESOURCES += \
    resources.qrc

DISTFILES +=
