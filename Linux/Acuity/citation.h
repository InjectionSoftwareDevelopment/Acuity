#ifndef CITATION_H
#define CITATION_H

#include <QMainWindow>
#include <QWebView>
#include <QtWebKit>
#include <QtWebKitWidgets/QWebFrame>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QMenu>
#include <iostream>

using namespace std;

namespace Ui {
class citation;
}

class citation : public QMainWindow
{
    Q_OBJECT

public:
    explicit citation(QWidget *parent = 0);
    ~citation();

private slots:
    void on_sourceSelectButton_clicked();

    void selectWiki();

    void selectDict();

    void selectEncyc();

    void on_webView_loadFinished(bool arg1);

    void on_generateCitation_clicked();

    void on_mlaButton_clicked();

    void on_apaButton_clicked();

    void encySet();



private:
    Ui::citation *ui;



    QMenu * sourceMenu;
    QAction * wikiButton;
    QAction * dictButton;
    QAction * encycButton;

    QLabel * sourceLabel;
    QLineEdit * searchedTerm;

    QTimer *timer;

    QString term;


    bool wiki;
    bool ency;
    bool dict;

    bool MLA;
    bool APA;

};

#endif // CITATION_H
