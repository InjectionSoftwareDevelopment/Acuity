#include "citation.h"
#include "ui_citation.h"

citation::citation(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::citation)
{
    ui->setupUi(this);
    this->setFixedSize(QSize(764, 480));
    ui->webView->hide();

    setWindowTitle("Citation");

    ui->sourceLabel->setText("Select a source...");

    ui->apaButton->setStyleSheet("background-color: #FFFFFF; border-radius: 5; color: black");
    ui->mlaButton->setStyleSheet("background-color: #FFFFFF; border-radius: 5; color: black");
    ui->sourceSelectButton->setStyleSheet("background-color: #FFFFFF; border-radius: 5; color: black");
    ui->generateCitation->setStyleSheet("background-color: #FFFFFF; border-radius: 5; color: green; font-weight: bold;");
    ui->searchedTerm->setStyleSheet("border-radius: 5;");
    ui->sourceLabel->setStyleSheet("font-weight: bold;");


    QPixmap sourceIcon(":/sourceicon.png");
    QIcon SourceButtonIcon(sourceIcon);
    ui->sourceSelectButton->setIcon(SourceButtonIcon);
    ui->sourceSelectButton->setIconSize(QSize(25,25));

}

citation::~citation()
{
    delete ui;
}

//Parse HTML//
void citation::on_webView_loadFinished(bool arg1)
{
    QVariant termJS;
    QVariant performSubmitJS;

    term.replace(" ", "_");

    termJS = ui->webView->page()->mainFrame()->evaluateJavaScript("var inputFields = document.querySelectorAll(\"input[type='text']\"); \
                                                                                                                 for (var i = inputFields.length >>> 0; i--;) { inputFields[i].value = '"+term+"';}");
    performSubmitJS = ui->webView->page()->mainFrame()->evaluateJavaScript("var passFields = document.querySelectorAll(\"input[type='submit']\"); \
                                                                           passFields[0].click()");





       timer = new QTimer(this);

       connect(timer, SIGNAL(timeout()), this, SLOT(encySet()));

       timer->start(2000);
}

void citation::encySet(){

    QWebElementCollection extract = ui->webView->page()->mainFrame()->findAllElements("body");

    foreach(QWebElement elemento, extract){
        QString filter = elemento.toPlainText();



        if(filter.contains(QString("Warning: "))){

            filter = "An error occurred while creating this citation, please make sure the term you used was correct.";
            ui->citationText->setText(filter);
            cout << "Reached error statement" << endl;

        }else{


            filter.replace("&quot;", "\"");
            ui->citationText->setText(filter);


        }


        timer->stop();


    }


}

void citation::on_sourceSelectButton_clicked()
{
    //Makes QMenu object
sourceMenu = new QMenu();


//Set context, add action buttons to menu, and enable a pop-up//
setContextMenuPolicy(Qt::CustomContextMenu);


wikiButton = sourceMenu->addAction("Wikipedia");
dictButton = sourceMenu->addAction("Dictionary");
encycButton = sourceMenu->addAction("Encyclopedia");
connect(this,SIGNAL(customContextMenuRequested(const QPoint)),this,SLOT(contextMenuRequested(QPoint)));
connect(wikiButton,SIGNAL(triggered()),this,SLOT(selectWiki()));
connect(dictButton,SIGNAL(triggered()),this,SLOT(selectDict()));
connect(encycButton,SIGNAL(triggered()),this,SLOT(selectEncyc()));

     QPoint menuPoint = ui->sourceSelectButton->pos();

    sourceMenu->popup(mapToGlobal(menuPoint));

}




void citation::selectWiki(){

    wiki = true;
    ency = false;
    dict = false;

    ui->sourceLabel->setText("Wikipedia");



}

void citation::selectDict(){

    wiki = false;
    ency = false;
    dict = true;

   ui->sourceLabel->setText("Dictionary");

}


void citation::selectEncyc(){

    wiki = false;
    ency = true;
    dict = false;

    ui->sourceLabel->setText("Encyclopedia");

}





void citation::on_generateCitation_clicked()
{
  term = ui->searchedTerm->text();
if(MLA == true){
   if(wiki == true){

       ui->citationText->setText(QString("\""+ term +"\". Wikipedia: The Free Encyclopedia. Wikimedia Foundation, Inc. 23.September.2014. Web. " + term + ". <http://en.wikipedia.org/wiki/"+ term + ">."));

   }else if(ency == true){

       ui->citationText->setText(QString("\""+ term +"\". Gale Encyclopedia of E-Commerce. 2002. Encyclopedia.com. 2015. Web. " + term + ". <http://www.encyclopedia.com/topic/"+ term +".aspx>."));

   }else if(dict == true){

       ui->citationText->setText(QString("\"" + term + "\". The American Heritage® Dictionary of Idioms by Christine Ammer. Houghton Mifflin Company. 2015. Web ." + term + ". <http://dictionary.reference.com/browse/"+ term +">."));
   }else{

       ui->citationText->setText(QString("Please select a source..."));
   }
}else if(APA == true){

    if(wiki == true){

        ui->citationText->setText(QString(term + ". (n.d.). Wikipedia: The Free Encyclopedia. Wikimedia Foundation, Inc. Retrieved" + term + "from http://en.wikipedia.org/wiki/" + term +"."));

    }else if(ency == true){

        ui->citationText->setText(QString("Generating..."));
        QString url = "http://acuity.injecti0n.org/encycAPA.php";
        ui->webView->load(QUrl(url));


    }else if(dict == true){

        ui->citationText->setText(QString(term + ". 1995. The American Heritage® Dictionary of Idioms by Christine Ammer. Houghton Mifflin Company. Retrieved" + term + "from http://dictionary.reference.com/browse/" + term +"."));
    }else{

        ui->citationText->setText(QString("Please select a source..."));
    }

}

}

void citation::on_mlaButton_clicked()
{
    MLA = true;
    APA = false;
        
   ui->mlaButton->setStyleSheet("background-color: #0099FF; border-radius: 5; color: black");
   ui->apaButton->setStyleSheet("background-color: #FFFFFF; border-radius: 5; color: black");
}

void citation::on_apaButton_clicked()
{
    MLA = false;
    APA = true;

    ui->mlaButton->setStyleSheet("background-color: #FFFFFF; border-radius: 5; color: black");
    ui->apaButton->setStyleSheet("background-color: #0099FF; border-radius: 5; color: black");

}
